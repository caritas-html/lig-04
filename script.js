const instructions = document.getElementById("instructions-button")
const instructionsBox = document.querySelector(".instructions-box")
const columns = document.querySelectorAll(".flex-col")
const draw = document.getElementById("draw-message")
const victory = document.getElementById("victory-message")
const player = document.getElementById("currentPlayer")
const resetButton = document.getElementById("reset-button")
const playerWinner = document.getElementById("playerWinner")


let currentPlayer = "playerOne",
    clickedColumn,
    lastNullID, cell


const horizontalWinCondition = (position, clickedColumn) => {
    let clickedColumnID = parseInt(clickedColumn.classList[1]),
        currentPlayer = document.getElementById(position).firstChild.classList[0]
        
    if (clickedColumnID === 1) {
        let playerPositionRight1 = document.getElementById(position + 1).firstChild,
            playerPositionRight2 = document.getElementById(position + 2).firstChild,
            playerPositionRight3 = document.getElementById(position + 3).firstChild

        if (playerPositionRight1 !== null 
            && 
            playerPositionRight2 !== null 
            && 
            playerPositionRight3 !== null) 
        {
            currentPlayer = document.getElementById(position).firstChild.classList[0]
            playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]
            playerPositionRight2 = document.getElementById(position + 2).firstChild.classList[0]
            playerPositionRight3 = document.getElementById(position + 3).firstChild.classList[0]

            if (currentPlayer === playerPositionRight1 
                && 
                currentPlayer === playerPositionRight2 
                && 
                currentPlayer === playerPositionRight3) 
            {
                return true
            }
        }
    }

    if (clickedColumnID === 2) {
        let playerPositionLeft1 = document.getElementById(position - 1).firstChild,
            playerPositionRight1 = document.getElementById(position + 1).firstChild,
            playerPositionRight2 = document.getElementById(position + 2).firstChild,
            playerPositionRight3 = document.getElementById(position + 3).firstChild

        if (playerPositionLeft1 !== null 
            && 
            playerPositionRight1 !== null 
            && 
            playerPositionRight2 !== null)
        {   
            playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]
            playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]
            playerPositionRight2 = document.getElementById(position + 2).firstChild.classList[0]

            if (currentPlayer === playerPositionLeft1 
                && 
                currentPlayer === playerPositionRight1 
                && 
                currentPlayer === playerPositionRight2) 
            {
                return true
            }
        }

        if (playerPositionRight1 !== null 
            && 
            playerPositionRight2 !== null 
            && 
            playerPositionRight3 !== null)
        {   
            playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]
            playerPositionRight2 = document.getElementById(position + 2).firstChild.classList[0]
            playerPositionRight3 = document.getElementById(position + 3).firstChild.classList[0]

            if (currentPlayer === playerPositionRight1 
                && 
                currentPlayer === playerPositionRight2 
                && 
                currentPlayer === playerPositionRight3) 
            {
                return true
            }
        }
    }

    if (clickedColumnID === 3) {
        let playerPositionLeft1 = document.getElementById(position - 1).firstChild,
            playerPositionLeft2 = document.getElementById(position - 2).firstChild,
            playerPositionRight1 = document.getElementById(position + 1).firstChild,
            playerPositionRight2 = document.getElementById(position + 2).firstChild,
            playerPositionRight3 = document.getElementById(position + 3).firstChild

        if (playerPositionLeft1 !== null 
            && 
            playerPositionLeft2 !== null 
            && 
            playerPositionRight1 !== null)
        {   
            playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]
            playerPositionLeft2 = document.getElementById(position - 2).firstChild.classList[0]
            playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]

            if (currentPlayer === playerPositionLeft1 
                && 
                currentPlayer === playerPositionLeft2 
                && 
                currentPlayer === playerPositionRight1) 
            {
                return true
            }
        }

        if (playerPositionRight1 !== null 
            && 
            playerPositionRight2 !== null 
            && 
            playerPositionRight3 !== null)
        {   
            playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]
            playerPositionRight2 = document.getElementById(position + 2).firstChild.classList[0]
            playerPositionRight3 = document.getElementById(position + 3).firstChild.classList[0]

            if (currentPlayer === playerPositionRight1 
                && 
                currentPlayer === playerPositionRight2 
                && 
                currentPlayer === playerPositionRight3) 
            {
                return true
            }
        }

        if (playerPositionLeft1 !== null 
            && 
            playerPositionRight1 !== null 
            && 
            playerPositionRight2 !== null)
        {   
            playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]
            playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]
            playerPositionRight2 = document.getElementById(position + 2).firstChild.classList[0]

            if (currentPlayer === playerPositionLeft1 
                && 
                currentPlayer === playerPositionRight1 
                && 
                currentPlayer === playerPositionRight2) 
            {
                return true
            }
        }
    }

    if (clickedColumnID === 4) {
        let playerPositionLeft1 = document.getElementById(position - 1).firstChild,
            playerPositionLeft2 = document.getElementById(position - 2).firstChild,
            playerPositionLeft3 = document.getElementById(position - 3).firstChild,
            playerPositionRight1 = document.getElementById(position + 1).firstChild,
            playerPositionRight2 = document.getElementById(position + 2).firstChild,
            playerPositionRight3 = document.getElementById(position + 3).firstChild
            
        if (playerPositionLeft1 !== null 
            && 
            playerPositionLeft2 !== null 
            && 
            playerPositionLeft3 !== null)
        {
            playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]
            playerPositionLeft2 = document.getElementById(position - 2).firstChild.classList[0]
            playerPositionLeft3 = document.getElementById(position - 3).firstChild.classList[0]

            if (currentPlayer === playerPositionLeft1 
                && 
                currentPlayer === playerPositionLeft2 
                && 
                currentPlayer === playerPositionLeft3) 
            {
                return true
            }
        }

        if (playerPositionRight1 !== null
                &&
                playerPositionRight2 !== null
                &&
                playerPositionRight3 !== null)
            {
                playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]
                playerPositionRight2 = document.getElementById(position + 2).firstChild.classList[0]
                playerPositionRight3 = document.getElementById(position + 3).firstChild.classList[0]

                if (currentPlayer === playerPositionRight1 
                    && 
                    currentPlayer === playerPositionRight2 
                    && 
                    currentPlayer === playerPositionRight3) 
                {
                    return true
                }
            }

            if (playerPositionRight1 !== null
                &&
                playerPositionLeft1 !== null
                &&
                playerPositionLeft2 !== null)
            {
                playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]
                playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]
                playerPositionLeft2 = document.getElementById(position - 2).firstChild.classList[0]

                if (currentPlayer === playerPositionRight1 
                    && 
                    currentPlayer === playerPositionLeft1 
                    && 
                    currentPlayer === playerPositionLeft2) 
                {
                    return true
                }
            }

            if (playerPositionRight1 !== null
                &&
                playerPositionRight2 !== null
                &&
                playerPositionLeft1 !== null)
            {
                playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]
                playerPositionRight2 = document.getElementById(position + 2).firstChild.classList[0]
                playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]

                if (currentPlayer === playerPositionRight1 
                    && 
                    currentPlayer === playerPositionRight2 
                    && 
                    currentPlayer === playerPositionLeft1) 
                {
                    return true
                }
            }
    }

    if (clickedColumnID === 5) {
        let playerPositionLeft1 = document.getElementById(position - 1).firstChild,
            playerPositionLeft2 = document.getElementById(position - 2).firstChild,
            playerPositionLeft3 = document.getElementById(position - 3).firstChild,
            playerPositionRight1 = document.getElementById(position + 1).firstChild,
            playerPositionRight2 = document.getElementById(position + 2).firstChild

        if (playerPositionLeft1 !== null 
            && 
            playerPositionRight1 !== null 
            && 
            playerPositionRight2 !== null)
        {
            playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]
            playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]
            playerPositionRight2 = document.getElementById(position + 2).firstChild.classList[0]

            if (currentPlayer === playerPositionLeft1 
                && 
                currentPlayer === playerPositionRight1 
                && 
                currentPlayer === playerPositionRight2) 
            {
                return true
            }
        }

        if (playerPositionLeft1 !== null 
            && 
            playerPositionLeft2 !== null 
            && 
            playerPositionLeft3 !== null)
        {   
            playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]
            playerPositionLeft2 = document.getElementById(position - 2).firstChild.classList[0]
            playerPositionLeft3 = document.getElementById(position - 3).firstChild.classList[0]

            if (currentPlayer === playerPositionLeft1 
                && 
                currentPlayer === playerPositionLeft2 
                && 
                currentPlayer === playerPositionLeft3) 
            {
                return true
            }
        }

        if (playerPositionRight1 !== null 
            && 
            playerPositionLeft1 !== null 
            && 
            playerPositionLeft2 !== null)
        {   
            playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]
            playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]
            playerPositionLeft2 = document.getElementById(position - 2).firstChild.classList[0]

            if (currentPlayer === playerPositionRight1 
                && 
                currentPlayer === playerPositionLeft1 
                && 
                currentPlayer === playerPositionLeft2) 
            {
                return true
            }
        }
    }

    if (clickedColumnID === 6) {
        let playerPositionLeft1 = document.getElementById(position - 1).firstChild,
            playerPositionLeft2 = document.getElementById(position - 2).firstChild,
            playerPositionLeft3 = document.getElementById(position - 3).firstChild,
            playerPositionRight1 = document.getElementById(position + 1).firstChild
            
        if (playerPositionLeft1 !== null 
            && 
            playerPositionLeft2 !== null 
            && 
            playerPositionRight1 !== null)
        {
            playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]
            playerPositionLeft2 = document.getElementById(position - 2).firstChild.classList[0]
            playerPositionRight1 = document.getElementById(position + 1).firstChild.classList[0]

            if (currentPlayer === playerPositionLeft1 
                && 
                currentPlayer === playerPositionLeft2 
                && 
                currentPlayer === playerPositionRight1) 
            {
                return true
            }
        }

        if (playerPositionLeft1 !== null 
            && 
            playerPositionLeft2 !== null 
            && 
            playerPositionLeft3 !== null)
        {   
            playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]
            playerPositionLeft2 = document.getElementById(position - 2).firstChild.classList[0]
            playerPositionLeft3 = document.getElementById(position - 3).firstChild.classList[0]

            if (currentPlayer === playerPositionLeft1 
                && 
                currentPlayer === playerPositionLeft2 
                && 
                currentPlayer === playerPositionLeft3) 
            {
                return true
            }
        }
    }

    if (clickedColumnID === 7) {
        let playerPositionLeft1 = document.getElementById(position - 1).firstChild,
            playerPositionLeft2 = document.getElementById(position - 2).firstChild,
            playerPositionLeft3 = document.getElementById(position - 3).firstChild
            
        if (playerPositionLeft1 !== null 
            && 
            playerPositionLeft2 !== null 
            && 
            playerPositionLeft3 !== null)
        {
            playerPositionLeft1 = document.getElementById(position - 1).firstChild.classList[0]
            playerPositionLeft2 = document.getElementById(position - 2).firstChild.classList[0]
            playerPositionLeft3 = document.getElementById(position - 3).firstChild.classList[0]

            if (currentPlayer === playerPositionLeft1 
                && 
                currentPlayer === playerPositionLeft2 
                && 
                currentPlayer === playerPositionLeft3) 
            {
                return true
            }
        }
    }
    return false
}

const verticalWinCondition = (position) =>{
    const lineDown = 7
    if(document.getElementById(position + lineDown * 3) !== null){
        const currentPlayer = document.getElementById(position).firstChild.classList[0]
        const PlayerPositionDown1 = document.getElementById(position + lineDown).firstChild.classList[0]
        const PlayerPositionDown2 = document.getElementById(position + lineDown * 2).firstChild.classList[0] 
        const PlayerPositionDown3 = document.getElementById(position + lineDown * 3).firstChild.classList[0]  
        
        if(currentPlayer === PlayerPositionDown1 && currentPlayer === PlayerPositionDown2 && currentPlayer === PlayerPositionDown3) {
            return true
        }
    }
    return false
}
    
const topLeftWinCondition = (position, currentPlayer)=>{
    const positionTopLeft1 = position - 7 - 1
    const positionTopLeft2 = position - 7 * 2 - 2
    const positionTopLeft3 = position - 7 * 3 - 3
    const positionDownRigth1 = position + 7 + 1
   
    const firstRowPosition = Math.ceil(position / 7)
    const lastRowPosition = Math.ceil(positionTopLeft3 / 7)
    
    if((positionTopLeft3 >= 1) && firstRowPosition - lastRowPosition === 3){
       if(  (document.getElementById(positionTopLeft1).firstChild !== null)
            &&
            (document.getElementById(positionTopLeft2).firstChild!==null)
            &&
            (document.getElementById(positionTopLeft3).firstChild!==null)
            
            
            
       ){

           const playerInTopLeft1 = document.getElementById(positionTopLeft1).firstChild.classList[0]        
           const playerInTopLeft2 = document.getElementById(positionTopLeft2).firstChild.classList[0]            
           const playerInTopLeft3 = document.getElementById(positionTopLeft3).firstChild.classList[0]

       
            if(currentPlayer === playerInTopLeft1 && currentPlayer === playerInTopLeft2  && currentPlayer=== playerInTopLeft3){
                return true
            }
      
        }
    }
    
    if(positionDownRigth1 <= 42 && positionTopLeft2 >=1){

        if( (document.getElementById(positionTopLeft1).firstChild !== null)
            &&
            (document.getElementById(positionTopLeft2).firstChild!==null)
            &&
            (document.getElementById(positionDownRigth1).firstChild!==null)
        ){
            
            const playerInTopLeft1 = document.getElementById(positionTopLeft1).firstChild.classList[0]        
            const playerInTopLeft2 = document.getElementById(positionTopLeft2).firstChild.classList[0]  
            const playerInDownRigth1 = document.getElementById(positionDownRigth1).firstChild.classList[0]
            
            if(currentPlayer === playerInTopLeft1  && currentPlayer === playerInTopLeft2 && currentPlayer === playerInDownRigth1){
                return true
            }
        }
    }
    
    return false    
}

const topRigthWinCondition = (position, currentPlayer) =>{

    const positionTopRigth1 = position - 7 + 1
    const positionTopRigth2 = position - 7 * 2 + 2
    const positionTopRigth3 = position - 7 * 3 + 3
    const positionDownLeft1 = position + 7 - 1
 
    const firstRowPosition = Math.ceil(position / 7)
    const lastRowPosition = Math.ceil(positionTopRigth3 / 7)

    if(positionTopRigth3 >=1 && firstRowPosition - lastRowPosition === 3){
        if( (document.getElementById(positionTopRigth1).firstChild !== null)
                &&
            (document.getElementById(positionTopRigth2).firstChild!==null)
                &&
            (document.getElementById(positionTopRigth3).firstChild!==null)
        

        ){            
            const playerInTopRigth1 = document.getElementById(positionTopRigth1).firstChild.classList[0]
            const playerInTopRigth2 = document.getElementById(positionTopRigth2).firstChild.classList[0]
            const playerInTopRigth3 = document.getElementById(positionTopRigth3).firstChild.classList[0]
            
            
            if(currentPlayer === playerInTopRigth1 && currentPlayer === playerInTopRigth2 && currentPlayer === playerInTopRigth3){
                return true
            }
        }
    }
    
    if(positionDownLeft1 <= 42 && positionTopRigth2 >=1){
        if((document.getElementById(positionTopRigth1).firstChild !== null)
            &&
            (document.getElementById(positionTopRigth2).firstChild!==null)
            &&
            (document.getElementById(positionDownLeft1).firstChild!==null)

        ){
            const playerInTopRigth1 = document.getElementById(positionTopRigth1).firstChild.classList[0]
            const playerInTopRigth2 = document.getElementById(positionTopRigth2).firstChild.classList[0]
            const playerInDownLeft1 = document.getElementById(positionDownLeft1).firstChild.classList[0]
            if(currentPlayer === playerInTopRigth1  && currentPlayer === playerInTopRigth2 && currentPlayer === playerInDownLeft1){
                return true
            }
        }
        
    }
    return false   
}

const downLeftWinCondition = (position, currentPlayer) =>{
    const positionDownLeft1 = position + 7 - 1
    const positionDownLeft2 = position + 7 * 2 - 2
    const positionDownLeft3 = position + 7 * 3 - 3
    const positionTopRigth1 = position - 7 + 1


    const firstRowPosition = Math.ceil(position / 7)
    const lastRowPosition = Math.ceil(positionDownLeft3 / 7)

    if((positionDownLeft3 <= 42) && firstRowPosition - lastRowPosition === -3){
        if(  (document.getElementById(positionDownLeft1).firstChild !== null)
             &&
             (document.getElementById(positionDownLeft2).firstChild!==null)
             &&
             (document.getElementById(positionDownLeft3).firstChild!==null)
             
             
             
        ){
        
            const playerInDownLeft1 = document.getElementById(positionDownLeft1).firstChild.classList[0]
            const playerInDownLeft2 = document.getElementById(positionDownLeft2).firstChild.classList[0]
            const playerInDownLeft3 = document.getElementById(positionDownLeft3).firstChild.classList[0]
            
            if(currentPlayer === playerInDownLeft1 && currentPlayer === playerInDownLeft2 && currentPlayer === playerInDownLeft3){
                return true
            }
        }
    }
    if(positionTopRigth1 >= 1 && positionDownLeft2 <= 42){

        if( (document.getElementById(positionDownLeft1).firstChild !== null)
            &&
            (document.getElementById(positionDownLeft2).firstChild!==null)
            &&
            (document.getElementById(positionTopRigth1).firstChild!==null)
        ){    
            const playerInDownLeft1 = document.getElementById(positionDownLeft1).firstChild.classList[0]
            const playerInDownLeft2 = document.getElementById(positionDownLeft2).firstChild.classList[0]
            const playerInTopRigth1 = document.getElementById(positionTopRigth1).firstChild.classList[0]
            if(currentPlayer === playerInDownLeft1  && currentPlayer === playerInDownLeft2 && currentPlayer === playerInTopRigth1){
                return true
            }
        }
    }
    return false   
}

const downRigthWinCondition = (position, currentPlayer) =>{
    const positionDownRigth1 = position + 7 + 1
    const positionDownRigth2 = position + 7 * 2 + 2
    const positionDownRigth3 = position + 7 * 3 + 3
    const positionTopLeft1 = position - 7 - 1 
    

    const firstRowPosition = Math.ceil(position / 7)
    const lastRowPosition = Math.ceil(positionDownRigth3 / 7)
    
    if((positionDownRigth3 <= 42) && firstRowPosition - lastRowPosition === -3){
       if(  (document.getElementById(positionDownRigth1).firstChild !== null)
            &&
            (document.getElementById(positionDownRigth2).firstChild!==null)
            &&
            (document.getElementById(positionDownRigth3).firstChild!==null)
            
            
            
       ){
            const playerInDownRigth1 = document.getElementById(positionDownRigth1).firstChild.classList[0]
            const playerInDownRigth2 = document.getElementById(positionDownRigth2).firstChild.classList[0]
            const playerInDownRigth3 = document.getElementById(positionDownRigth3).firstChild.classList[0]
            
            
            if(currentPlayer === playerInDownRigth1  && currentPlayer=== playerInDownRigth2 && currentPlayer === playerInDownRigth3){
                return true
            }
        }
    }

    if(positionTopLeft1 >= 1 && positionDownRigth2 <= 42){

        if( (document.getElementById(positionDownRigth1).firstChild !== null)
            &&
            (document.getElementById(positionDownRigth2).firstChild!==null)
            &&
            (document.getElementById(positionTopLeft1).firstChild!==null)
        ){
            const playerInDownRigth1 = document.getElementById(positionDownRigth1).firstChild.classList[0]
            const playerInDownRigth2 = document.getElementById(positionDownRigth2).firstChild.classList[0]
            const playerInTopLeft1 = document.getElementById(positionTopLeft1).firstChild.classList[0]
            if(currentPlayer === playerInDownRigth1 && currentPlayer === playerInDownRigth2 && currentPlayer === playerInTopLeft1){
                return true
            }
        }
    }

    return false   

}


const diagonalWinCondition = (position, currentPlayer) =>{
   
    const topLeftWin = topLeftWinCondition(position, currentPlayer)
    const topRigthWin = topRigthWinCondition(position, currentPlayer)
    const downLeftWin = downLeftWinCondition(position, currentPlayer)
    const downRigthWin = downRigthWinCondition(position, currentPlayer)
    
    if(topLeftWin === true   || topRigthWin === true  || downLeftWin === true || downRigthWin === true){
        return true
    }
    return false
}


function swapPlayer(currentPlayer) {
    if (currentPlayer === "playerTwo") {
        return "playerOne"
    }

    return "playerTwo"
}

const handleClick = (event) => {
    const divPlayerOne = document.createElement("div")
    divPlayerOne.classList.add("playerOne", "spinning", "growing")

    const divPlayerTwo = document.createElement("div")
    divPlayerTwo.classList.add("playerTwo", "spinning", "growing")

    clickedColumn = event.currentTarget
    lastNullID = parseInt(clickedColumn.lastChild.previousSibling.id)

    for (let i = 0; i < clickedColumn.childElementCount; i++) {
        cell = document.getElementById(lastNullID)

        if (cell.childElementCount === 0) {
            if (currentPlayer === "playerOne") {
                cell.appendChild(divPlayerOne)
            }

            else if (currentPlayer === "playerTwo") {
                cell.appendChild(divPlayerTwo)
            }
            
            break
        }

        lastNullID -= 7
    }

    if(currentPlayer === "playerOne"){
        
        player.innerText = "Player Two"

    }else{
       
        player.innerText = "Player One"
    }
    
    if(lastNullID <= 42 && lastNullID >=1){

        let diagonalWin = diagonalWinCondition(lastNullID, currentPlayer)
        let verticalWin = verticalWinCondition(lastNullID, currentPlayer)
        let horizontalWin = horizontalWinCondition(lastNullID, clickedColumn)
        let drawWin = drawGame()

        if(diagonalWin === true || verticalWin === true || horizontalWin === true){
            
            if (currentPlayer === "playerTwo") {
                playerWinner.innerText = "Player Two"
                victory.appendChild(divPlayerTwo)
            }

            else if (currentPlayer === "playerOne") {              
                playerWinner.innerText = "Player One"
                victory.appendChild(divPlayerOne)
            }
            victory.classList.remove("display")
        }

        if(drawWin === true){
            
            draw.classList.remove("display")
        }

        currentPlayer = swapPlayer(currentPlayer)

    }
 
}

const  drawGame = () =>{
    const totalMovePlayerOne = document.getElementsByClassName("playerOne").length
    const totalMovePlayerTwo = document.getElementsByClassName("playerTwo").length
    const maxMove = 42
    if( totalMovePlayerOne + totalMovePlayerTwo === maxMove ){
        return true
    }

    return false
}

const resetGame = () =>{
    const allMovePlayerOne = document.getElementsByClassName("playerOne")
    const allMovePlayerTwo = document.getElementsByClassName("playerTwo")

    while(allMovePlayerOne.length > 0){
        allMovePlayerOne[0].remove()
    }

    while(allMovePlayerTwo.length > 0){
        allMovePlayerTwo[0].remove()
    }

    currentPlayer = "playerOne"
    player.innerText = "Player One"
    playerWinner.innerText = ""
    victory.classList.add("display")
    draw.classList.add("display")
}

const showInfos = () => {
    instructionsBox.classList.toggle("hidden")
}

resetButton.addEventListener("click", resetGame)

instructions.addEventListener("click", showInfos)

for (let i = 0; i < columns.length; i++) {
    columns[i].addEventListener("click", handleClick)
}